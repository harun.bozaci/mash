#include <iostream>
#include <utility>

template<typename T>
class Memory
{
public:
	Memory() : Memory{ 0 } {}
	Memory(T const& val) : m_val{ val } {}
	void operator=(const Memory& mem)
	{
		m_val = mem.m_val;
	}

	T& getMemVal(void)&
	{
		return m_val;
	}

	T getMemVal(void) const&&
	{
		return m_val;
	}

protected:
	T m_val;
};

template<typename T, size_t n>
class Memory<T[n]>
{
public:
	T& operator[](size_t k)
	{
		return m_val[k];
	}
protected:
	T m_val[n];
};

template<typename T, bool IsAllowed>
class Register : public Memory<T>
{
public:
	Register(const T& val) : BaseC{ val } {}
private:
	using BaseC = Memory<T>;
};

template <typename T>
class GPRegister : public Register<T, true>
{
public:
	GPRegister() : GPRegister{ 0 } {}
	GPRegister(const T& val) : BaseC{ val } {}
private:
	using BaseC = Register<T, true>;
};

template <typename T>
class SPRegister : public Register < T, false>
{
public:
	SPRegister() : SPRegister{ 0 } {}
	SPRegister(const T& val) : BaseC{ val } {}
private:
	using BaseC = Register<T, false>;
};

using Word = unsigned short;
using Byte = unsigned char;
enum class OPS : Word
{
	MOV,
	ADD,
	SUB,
};

template<size_t Cap>
class RAM : Memory<Byte[Cap * 1024]>
{
public:
	RAM() {}

	Byte& operator[](size_t n)
	{
		return this->m_val[n];
	}
	void operator*(void)
	{
		std::cout << "deneme";
		for (size_t i = 0; i < 10; ++i)
			std::cout << (unsigned)this->m_val[i] << " ";
		std::cout << '\n';
	}
private:
};

template<size_t Cap>
class Storage : Memory<unsigned char[Cap * 1024]>
{
public:
	Storage()
	{
	}
private:
};

struct Instructions
{
	OPS operation;
	Memory<unsigned short> operand;
};

Instructions program[] = { {OPS::MOV, 1}
						 , {OPS::ADD, 2}
						 , {OPS::ADD, 3} };

class CPU
{
public:
	struct Flags
	{
		unsigned char Zero : 1;
		unsigned char Negative : 1;
		unsigned char Carry : 1;
		unsigned char Overflow : 1;
		unsigned char Trap : 1;
		unsigned char NA : 3;
		Flags() {
			resetFlags();
		}
		void resetFlags()
		{
			Zero = Negative = Carry = Overflow = Trap = 0;
		}
	};

	void fetch()
	{
	}
	void decode()
	{
	}
	void execute()
	{
	}


private:
	Flags flags;
	GPRegister<Word> R1;
	GPRegister<Word> R2;
	SPRegister<Word> SP;
	SPRegister<Word> BP;
	SPRegister<Word> IP;
};

class Machine
{
private:
	CPU c;
public:
	Machine()
	{
	}
};



#define EXECUTABLE  0
#if EXECUTABLE
int main(void)
{
	GPRegister<unsigned short> myreg{ 20 };
	GPRegister<unsigned short> urreg = myreg;
	GPRegister<unsigned short> itsreg = urreg;
	Memory<unsigned short> reg2{ myreg };
	Memory<unsigned short> reg1{ urreg };
	reg2 = 30;
	urreg = itsreg;
	urreg = 45;
	std::cout << itsreg.getMemVal() << " " << urreg.getMemVal() << "\n";
}
#endif

#if !EXECUTABLE
int main(void)
{
	RAM<1024> myram;
	*myram;
}
#endif